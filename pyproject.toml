[build-system]
requires = ["setuptools"]
build-backend = "setuptools.build_meta"

[project]
name = "kadi"
version = "1.4.0"
description = "A generic and open source virtual research environment."
authors = [{name = "Karlsruhe Institute of Technology"}]
readme = "README.md"
license = {text = "Apache-2.0"}
requires-python = ">=3.9,<3.14"
classifiers = [
  "Development Status :: 5 - Production/Stable",
  "License :: OSI Approved :: Apache Software License",
  "Operating System :: POSIX :: Linux",
  "Programming Language :: Python :: 3",
  "Programming Language :: JavaScript",
]
dependencies = [
  "aiobotocore==2.21.0",
  "alembic==1.14.1",
  "anonip==1.1.0",
  "Authlib==1.5.1",
  "Babel==2.17.0",
  "blinker==1.9.0",
  "celery==5.4.0",
  "charset-normalizer==3.4.1",
  "click==8.1.8",
  "cryptography==44.0.2",
  "defusedxml==0.7.1",
  "elastic-transport==8.17.0",
  "elasticsearch==8.17.1",
  "elasticsearch-dsl==8.17.1",
  "email-validator==2.1.1",
  "Flask==3.1.0",
  "flask-babel==4.0.0",
  "Flask-Limiter==3.10.1",
  "Flask-Login==0.6.3",
  "Flask-Migrate==4.1.0",
  # Version >=3.1 requires SQLAlchemy >=2.
  "Flask-SQLAlchemy==3.0.5",
  "flask-talisman==1.1.0",
  "Flask-WTF==1.2.2",
  "fonttools==4.56.0",
  "fpdf2==2.8.2",
  "h5py==3.13.0",
  "itsdangerous==2.2.0",
  "Jinja2==3.1.5",
  "jsonref==1.1.0",
  "kombu==5.4.2",
  "ldap3==2.9.1",
  "limits==4.0.1",
  "markdown-it-py==3.0.0",
  "MarkupSafe==3.0.2",
  "marshmallow==3.26.1",
  "mdit-py-plugins==0.4.2",
  "mdurl==0.1.2",
  "Pillow==11.1.0",
  "pluggy==1.5.0",
  "psycopg2==2.9.10",
  "pyasn1==0.6.1",
  "PyJWT==2.10.1",
  "python-dotenv==1.0.1",
  "python-magic==0.4.27",
  "qrcode==8.0",
  "rdflib==7.1.3",
  "redis==5.2.1",
  "requests==2.32.3",
  "s3fs==2025.2.0",
  "sentry-sdk==2.22.0",
  # Version >=2 requires larger refactorings.
  "SQLAlchemy==1.4.54",
  "SQLAlchemy-Utils==0.41.2",
  # Version >=2 requires Python >=3.10.
  "urllib3==1.26.20",
  "uWSGI==2.0.28",
  "Werkzeug==3.1.3",
  "WTForms==3.2.1",
  "zipstream-ng==1.8.0",
]

[project.optional-dependencies]
dev = [
  "aiosmtpd==1.4.6",
  "autoflake==2.3.1",
  "black==25.1.0",
  "build==1.2.2.post1",
  "isort==6.0.1",
  # Version >=4 requires Python >=3.10.
  "myst-parser==3.0.1",
  "pre-commit==4.0.1",
  "pylint==3.3.4",
  "pyproject-parser==0.13.0",
  "pytest==8.3.5",
  "pytest-cov==6.0.0",
  "rfc3339-validator==0.1.4",
  # Version >=8 requires Python >=3.10.
  "Sphinx==7.4.7",
  "sphinx_inline_tabs==2023.4.21",
  "sphinx-rtd-theme==3.0.2",
  # Version >=2024.2.24 requires Python >=3.10.
  "Sphinx-Substitution-Extensions==2022.2.16",
  "sphinxcontrib-httpdomain==1.8.1",
  "twine==6.1.0",
  "watchdog==6.0.0",
]

[project.urls]
Homepage = "https://kadi.iam.kit.edu"
Documentation = "https://kadi.readthedocs.io/en/stable"
Source = "https://gitlab.com/iam-cms/kadi"
Changelog = "https://gitlab.com/iam-cms/kadi/-/blob/master/HISTORY.md"

[project.scripts]
kadi = "kadi.cli.main:kadi"

[project.entry-points.kadi_plugins]
influxdb = "kadi.plugins.influxdb.plugin"
s3 = "kadi.plugins.s3.plugin"
tib_ts = "kadi.plugins.tib_ts.plugin"
zenodo = "kadi.plugins.zenodo.plugin"

[tool.setuptools]
license-files = ["LICENSE"]

[tool.setuptools.packages.find]
include = ["kadi", "kadi.*"]

[tool.pytest.ini_options]
norecursedirs = [
  ".*",
  "bin",
  "build",
  "config",
  "dist",
  "docs",
  "instance",
  "kadi",
  "node_modules",
  "*.egg-info",
]
filterwarnings = [
  "error",
  "ignore::sqlalchemy.exc.RemovedIn20Warning",
  "ignore:.*(tagMap|typeMap).*:DeprecationWarning:pyasn1",
]

[tool.coverage.run]
branch = true
source = ["kadi"]
omit = [
  "kadi/app.py",
  "kadi/cli/*",
  "kadi/migrations/*",
  "kadi/plugins/*",
  "kadi/vendor/*",
  "kadi/wsgi.py",
]

[tool.isort]
profile = "black"
force_single_line = true
lines_after_imports = 2

[tool.autoflake]
exclude = "kadi/migrations/**/*"
ignore-init-module-imports = true
remove-all-unused-imports = true

[tool.pylint."MAIN"]
bad-functions = "print"
load-plugins = "pylint.extensions.bad_builtin"
max-line-length = 88

[tool.pylint."MESSAGES CONTROL"]
disable = [
  "attribute-defined-outside-init",
  "bad-mcs-classmethod-argument",
  "bare-except",
  "broad-exception-caught",
  "consider-using-f-string",
  "cyclic-import",
  "duplicate-code",
  "import-outside-toplevel",
  "inconsistent-return-statements",
  "invalid-name",
  "missing-module-docstring",
  "no-member",
  "protected-access",
  "redefined-builtin",
  "redefined-outer-name",
  "subprocess-run-check",
  "too-few-public-methods",
  "too-many-ancestors",
  "too-many-arguments",
  "too-many-branches",
  "too-many-instance-attributes",
  "too-many-lines",
  "too-many-locals",
  "too-many-nested-blocks",
  "too-many-positional-arguments",
  "too-many-public-methods",
  "too-many-return-statements",
  "too-many-statements",
  "unused-argument",
  "wrong-import-order",
  "wrong-import-position",
]
