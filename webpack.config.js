/* Copyright 2020 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

import path from 'path';
import url from 'url';

import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import {VueLoaderPlugin} from 'vue-loader';
import {globSync} from 'glob';
import webpack from 'webpack';

const cssLoader = {
  loader: 'css-loader',
  options: {
    esModule: false,
  },
};

const currentDir = path.dirname(url.fileURLToPath(import.meta.url));
const scriptsDir = 'kadi/assets/scripts';
const entryPoints = {
  vendor: `${scriptsDir}/vendor.js`,
};

globSync(`${scriptsDir}/app/**/*.js`).forEach((fileName) => {
  entryPoints[fileName.substring(scriptsDir.length + 1, fileName.length - 3)] = fileName;
});

export default (env, argv) => {
  return {
    devtool: argv.mode === 'development' ? 'eval-source-map' : false,
    entry: entryPoints,
    optimization: {
      splitChunks: {
        chunks() {
          return false;
        },
      },
    },
    output: {
      chunkFilename: 'chunks/[name]-[contenthash].js',
      clean: true,
      devtoolFallbackModuleFilenameTemplate: 'webpack:///[resource-path]?[hash]',
      devtoolModuleFilenameTemplate: (info) => {
        return info.resourcePath.match(/\.vue$/) && info.allLoaders
          ? `webpack-generated:///${info.resourcePath}?${info.hash}`
          : `webpack:///${path.normalize(info.resourcePath)}`;
      },
      hashDigestLength: 32,
      hashFunction: 'md5',
      path: path.join(currentDir, 'kadi/static/dist'),
    },
    module: {
      rules: [
        {
          test: /\.m?js$/,
          resolve: {
            fullySpecified: false,
          },
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            compilerOptions: {
              whitespace: 'preserve',
            },
          },
        },
        {
          test: /\.css$/,
          use: [
            'style-loader',
            cssLoader,
            'postcss-loader',
          ],
        },
        {
          test: /\.scss$/,
          use: [
            'style-loader',
            cssLoader,
            'postcss-loader',
            'sass-loader',
          ],
        },
        {
          test: /\/styles\/main\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            cssLoader,
            'postcss-loader',
            'sass-loader',
          ],
        },
        {
          test: /.*\.(ttf|woff2?)$/,
          type: 'asset/resource',
          generator: {
            filename: 'fonts/[name]-[contenthash][ext]',
          },
        },
      ],
    },
    performance: {
      hints: false,
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'styles/main.css',
      }),
      new VueLoaderPlugin(),
      new webpack.DefinePlugin({
        __VUE_OPTIONS_API__: true,
        __VUE_PROD_DEVTOOLS__: false,
        __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: false,
      }),
    ],
    resolve: {
      alias: {
        vue: 'vue/dist/vue.esm-bundler.js',
      },
      modules: ['.', './node_modules'],
    },
  };
};
