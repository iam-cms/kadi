# Security policy

To report a potential security vulnerability, please prefer contacting one of
the current maintainers directly, if possible, or send an email to
[feedback-kadi4mat@lists.kit.edu](mailto:feedback-kadi4mat@lists.kit.edu).

Once a confirmed security vulnerability is fixed, a new release will be issued
as soon as possible. Note that security fixes will only be released for the
**latest** version of Kadi4Mat.
