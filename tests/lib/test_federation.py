# Copyright 2024 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.federation import get_federated_instance
from kadi.lib.federation import get_federated_instances


def test_get_federated_instances():
    """Test if federated instances are retrieved correctly."""
    instances = get_federated_instances()
    assert len(instances) == 1


def test_get_federated_instance():
    """Test if a specific federated instance is retrieved correctly."""
    provider_name = "test"
    provider = get_federated_instance(provider_name)

    assert provider
    assert provider["name"] == provider_name
