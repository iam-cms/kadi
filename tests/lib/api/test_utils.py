# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.api.utils import get_api_version
from kadi.lib.api.utils import is_api_request
from kadi.lib.api.utils import is_internal_api_request
from kadi.lib.web import url_for


@pytest.mark.parametrize(
    "endpoint,result",
    [("main.index", False), ("api.index", True), ("api.index_v1", True)],
)
def test_is_api_request(endpoint, result, client, user_session):
    """Test if API requests are detected correctly."""
    with user_session():
        client.get(url_for(endpoint))
        assert is_api_request() is result


@pytest.mark.parametrize(
    "endpoint,internal,result",
    [
        ("api.index", False, False),
        ("api.index", True, True),
        ("main.index", False, False),
        ("main.index", True, False),
    ],
)
def test_is_internal_api_request(endpoint, internal, result, client, user_session):
    """Test if internal API requests are detected correctly."""
    with user_session():
        values = {"_internal": True} if internal else {}
        client.get(url_for(endpoint, **values))

        assert is_internal_api_request() is result


@pytest.mark.parametrize(
    "endpoint,default,version",
    [
        ("main.index", None, None),
        ("main.index", "v1", None),
        ("api.index", None, None),
        ("api.index", "v1", "v1"),
        ("api.index_v1", None, "v1"),
    ],
)
def test_get_api_version(endpoint, default, version, client, user_session):
    """Test if API versions are determined correctly."""
    with user_session():
        client.get(url_for(endpoint))
        assert get_api_version(default) == version
