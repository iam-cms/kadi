# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO
from urllib.parse import urlparse

import pytest
from flask import current_app
from flask import session

import kadi.lib.constants as const
from kadi.lib.conversion import lower
from kadi.lib.conversion import normalize
from kadi.lib.web import download_bytes
from kadi.lib.web import get_locale
from kadi.lib.web import get_next_url
from kadi.lib.web import html_error_response
from kadi.lib.web import paginated
from kadi.lib.web import qparam
from kadi.lib.web import static_url
from kadi.lib.web import url_for
from tests.utils import check_view_response


def test_get_locale(client):
    """Test if determining the locale works correctly."""
    new_locale = "de"

    with client:
        # Outside of a request context.
        assert get_locale() == const.LOCALE_DEFAULT

        client.get(url_for("main.index"))
        assert get_locale() == const.LOCALE_DEFAULT

        # Using a cookie with a valid locale.
        client.set_cookie(const.LOCALE_COOKIE_NAME, new_locale)

        client.get(url_for("main.index"))
        assert get_locale() == new_locale

        # Using a cookie with an invalid locale.
        client.set_cookie(const.LOCALE_COOKIE_NAME, "test")

        client.get(url_for("main.index"))
        assert get_locale() == const.LOCALE_DEFAULT


@pytest.mark.parametrize(
    "data,length", [(b"test", 4), ([b"foo", b"bar"], None), (BytesIO(b"test"), 4)]
)
def test_download_bytes(data, length, request_context):
    """Test if downloading binary data works correctly."""
    filename = "test.txt"
    response = download_bytes(data, filename=filename)
    response.direct_passthrough = False

    assert response.status_code == 200
    assert response.content_length == length
    assert response.mimetype == const.MIMETYPE_TEXT
    assert response.headers["Content-Disposition"] == f"attachment; filename={filename}"


def test_paginated():
    """Test if the "paginated" decorator works correctly."""

    # pylint: disable=no-value-for-parameter
    @paginated
    def _paginated_func(page, per_page):
        assert page == 1
        assert per_page == 100

    with current_app.test_request_context("?page=-1&per_page=200"):
        _paginated_func()

    @paginated(page_max=5)
    def _paginated_func(page, per_page):
        assert page == 5
        assert per_page == 10

    with current_app.test_request_context("?page=100&per_page=test"):
        _paginated_func()


def test_qparam():
    """Test if the "qparam" decorator works correctly."""

    # pylint: disable=no-value-for-parameter
    @qparam("param_1")
    @qparam("param_2", type=const.QPARAM_TYPE_STR)
    @qparam("param_3", type=const.QPARAM_TYPE_INT)
    @qparam("param_4", type=const.QPARAM_TYPE_BOOL)
    @qparam("param_5", multiple=True)
    @qparam("param_6", multiple=True, default="ignored")
    @qparam("param_7", multiple=True, parse=int)
    @qparam("param_8", parse=[lower, normalize])
    @qparam("param_9", default=lambda: "default")
    def _parametrized_func(qparams):
        assert qparams["param_1"] == "foo"
        assert qparams["param_2"] == "bar"
        assert qparams["param_3"] == 1
        assert qparams["param_4"] is True
        assert qparams["param_5"] == ["a", "b", "c"]
        assert qparams["param_6"] == []
        assert qparams["param_7"] == [1, 2]
        assert qparams["param_8"] == "test"
        assert qparams["param_9"] == "default"

    with current_app.test_request_context(
        "?param_1=foo&param_1=bar"
        "&param_2=bar"
        "&param_3=1"
        "&param_4=true"
        "&param_5=a&param_5=b&param_5=c"
        "&param_7=1&param_7=a&param_7=2"
        "&param_8= Test "
    ):
        _parametrized_func()


def test_url_for(api_client, dummy_personal_token):
    """Test if URLs are generated correctly."""
    base_url = (
        f"{current_app.config['PREFERRED_URL_SCHEME']}://"
        f"{current_app.config['SERVER_NAME']}"
    )

    # Test the URL generation without a request context.
    assert url_for("main.index") == f"{base_url}/"
    assert url_for("api.index") == f"{base_url}/api"
    assert (
        url_for(f"api.index_{const.API_VERSION_DEFAULT}")
        == f"{base_url}/api/{const.API_VERSION_DEFAULT}"
    )
    assert (
        url_for(f"api.index_{const.API_VERSION_DEFAULT}", _ignore_version=True)
        == f"{base_url}/api/{const.API_VERSION_DEFAULT}"
    )

    # Test the URL generation with a request context.
    with api_client(dummy_personal_token) as _api_client:
        # Make a request to an endpoint without explicit API version.
        _api_client.get(url_for("api.index"))

        assert url_for("api.index") == f"{base_url}/api"

        # Make a request to an endpoint with an explicit API version.
        _api_client.get(url_for(f"api.index_{const.API_VERSION_DEFAULT}"))

        assert url_for("api.index") == f"{base_url}/api/{const.API_VERSION_DEFAULT}"
        assert url_for("api.index", _ignore_version=True) == f"{base_url}/api"


def test_static_url(monkeypatch):
    """Test if static URLs are generated correctly."""
    base_url = (
        f"{current_app.config['PREFERRED_URL_SCHEME']}://"
        f"{current_app.config['SERVER_NAME']}"
    )

    assert static_url("test") == f"{base_url}/static/test"

    monkeypatch.setitem(current_app.config, "MANIFEST_MAPPING", {"foo": "bar"})

    assert static_url("test") == f"{base_url}/static/test"
    assert static_url("foo") == f"{base_url}/static/bar"


def test_get_next_url(request_context):
    """Test if validating the redirect URL works correctly."""
    fallback_url = url_for("main.index")
    valid_url = url_for("records.records")
    invalid_url = "http://example.com"

    assert get_next_url() == fallback_url

    # Using an absolute URL.
    session[const.SESSION_KEY_NEXT_URL] = valid_url
    assert get_next_url() == valid_url

    # Using a relative URL.
    session[const.SESSION_KEY_NEXT_URL] = urlparse(valid_url).path
    assert get_next_url() == urlparse(valid_url).path

    # Using an invalid URL.
    session[const.SESSION_KEY_NEXT_URL] = invalid_url
    assert get_next_url() == fallback_url


def test_html_error_response(request_context):
    """Test if HTML error responses work correctly."""
    response = html_error_response(404, description="description")
    data = response.get_data(as_text=True)

    check_view_response(response, status_code=404)
    assert "404" in data
    assert "description" in data
