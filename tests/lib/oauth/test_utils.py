# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.oauth.core import create_oauth2_client_token
from kadi.lib.oauth.utils import get_oauth2_client_token
from kadi.lib.oauth.utils import get_oauth2_provider
from kadi.lib.oauth.utils import get_oauth2_providers
from kadi.lib.oauth.utils import has_oauth2_providers
from kadi.lib.utils import utcnow


def test_get_oauth2_client_token(dummy_user):
    """Test if OAuth2 client tokens are retrieved correctly."""
    name = "test"
    oauth2_client_token = create_oauth2_client_token(
        name=name, access_token="test", user=dummy_user
    )

    assert get_oauth2_client_token(name, user=dummy_user) == oauth2_client_token

    oauth2_client_token.expires_at = utcnow()

    assert get_oauth2_client_token(name, user=dummy_user) == oauth2_client_token
    assert not get_oauth2_client_token(name, user=dummy_user, refresh=True)


def test_has_oauth2_providers():
    """Test if OAuth2 providers are checked for correctly."""
    assert has_oauth2_providers()


def test_get_oauth2_providers(dummy_user):
    """Test if OAuth2 providers are retrieved correctly."""
    providers = get_oauth2_providers(user=dummy_user)
    # The federated instance and the Zenodo provider.
    assert len(providers) == 2


def test_get_oauth2_provider(dummy_user):
    """Test if a specific OAuth2 provider is retrieved correctly."""
    provider_name = "test"
    provider = get_oauth2_provider(provider_name, user=dummy_user)

    assert provider
    assert provider["name"] == provider_name
    assert not provider["is_connected"]
