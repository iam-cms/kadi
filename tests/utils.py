# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import kadi.lib.constants as const


def _check_response(response, status_code, content_type):
    assert response.status_code == status_code
    assert response.content_type == content_type


def check_view_response(response, status_code=200, content_type=None):
    """Check the status code and content type of a view response."""
    content_type = (
        content_type
        if content_type is not None
        else f"{const.MIMETYPE_HTML}; charset=utf-8"
    )
    _check_response(response, status_code, content_type)


def check_api_response(response, status_code=200, content_type=const.MIMETYPE_JSON):
    """Check the status code and content type of an API response."""
    _check_response(response, status_code, content_type)
