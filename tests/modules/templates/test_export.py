# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from urllib.parse import quote

import pytest
from flask import json
from rdflib import OWL
from rdflib import RDF
from rdflib import SH
from rdflib import BNode
from rdflib import Literal
from rdflib import Namespace
from rdflib import URIRef

import kadi.lib.constants as const
from kadi.lib.export import RDFGraph
from kadi.lib.web import url_for
from kadi.modules.records.extras import is_nested_type
from kadi.modules.templates.export import get_export_data
from kadi.modules.templates.models import TemplateType


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
def test_get_export_data_json(template_type, dummy_user, new_template):
    """Test if the template JSON export works correctly."""
    template = new_template(type=template_type)

    extra = {
        "key": "test",
        "type": "dict",
        "value": [{"key": "test", "type": "str", "value": None}],
    }
    filtered_extra = {"key": "test", "type": "dict", "value": []}

    if template_type == TemplateType.RECORD:
        template.data["extras"] = [extra]
    else:
        template.data = [extra]

    export_filter = {"user": True, "extras": {"test": {"test": {}}}}
    json_data = get_export_data(
        template, const.EXPORT_TYPE_JSON, export_filter=export_filter, user=dummy_user
    )

    assert json_data is not None

    template_data = json.loads(json_data.read().decode())

    if template_type == TemplateType.RECORD:
        assert "creator" not in template_data
        assert template_data["data"]["extras"] == [filtered_extra]
    else:
        assert template_data["data"] == [filtered_extra]


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
@pytest.mark.parametrize(
    "extra",
    [
        # String value with required validation and options.
        {
            "type": "str",
            "key": "test",
            "value": None,
            "validation": {"required": True, "options": ["test1", "test2"]},
        },
        # Integer value with a range validation.
        {
            "type": "int",
            "key": "test",
            "value": 1,
            "unit": None,
            "validation": {"range": {"min": 1, "max": 2}},
        },
        # Regular float value with a unit.
        {
            "type": "float",
            "key": "test",
            "value": None,
            "unit": "test",
        },
        # Regular boolean value.
        {
            "key": "test",
            "type": "bool",
            "value": None,
        },
        # Regular date value.
        {
            "key": "test",
            "type": "date",
            "value": None,
        },
        # Nested dictionary value.
        {
            "key": "test",
            "type": "dict",
            "value": [],
        },
        # Nested list value.
        {
            "key": "test",
            "type": "list",
            "value": [],
        },
    ],
)
def test_get_export_data_json_schema(template_type, extra, dummy_user, new_template):
    """Test if the template JSON Schema export works correctly."""
    template = new_template(type=template_type)

    if template_type == TemplateType.RECORD:
        template.data["extras"] = [extra]
    else:
        template.data = [extra]

    json_data = get_export_data(
        template, const.EXPORT_TYPE_JSON_SCHEMA, user=dummy_user
    )

    assert json_data is not None

    json_schema = json.loads(json_data.read().decode())

    assert "$schema" in json_schema

    for key, value in json_schema["properties"].items():
        assert key == extra["key"]
        assert "type" in value

        if "unit" in extra:
            assert "unit" in value

        if value["type"] == "object":
            assert "properties" in value
        elif value["type"] == "array":
            assert "prefixItems" in value

        validation = extra.get("validation", {})

        if "required" in validation:
            assert key in json_schema["required"]

        if (options := validation.get("options")) is not None:
            assert options == value["enum"]

        if (value_range := validation.get("range")) is not None:
            assert value_range.get("min") == value.get("minimum")
            assert value_range.get("max") == value.get("maximum")


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
@pytest.mark.parametrize(
    "extra",
    [
        # String value with options.
        {
            "key": "test",
            "type": "str",
            "value": "test",
            "validation": {"options": ["test1", "test2"]},
        },
        # String value with IRI validation.
        {
            "key": "test",
            "type": "str",
            "value": "https://example.com/string",
            "validation": {"iri": True},
        },
        # Integer value with a range validation.
        {
            "key": "test",
            "type": "int",
            "value": 1,
            "unit": None,
            "validation": {"range": {"min": 1, "max": 2}},
        },
        # Float value with a required validation.
        {
            "key": "test",
            "type": "float",
            "value": 1.23,
            "unit": "test",
            "validation": {"required": True},
        },
        # Nested dictionary value.
        {
            "key": "test",
            "term": "https://example.com/test",
            "type": "dict",
            "value": [
                {"key": "test1", "type": "str", "value": None},
                {"key": "test2", "type": "bool", "value": None},
            ],
        },
        # Empty nested dictionary value.
        {
            "key": "test",
            "type": "dict",
            "value": [],
        },
        # Nested list value.
        {
            "key": "test",
            "type": "list",
            "value": [
                {"type": "str", "value": None},
                {"type": "bool", "value": None},
            ],
        },
    ],
)
def test_get_export_data_shacl(template_type, extra, dummy_user, new_template):
    """Test if the template SHACL exports works correctly."""
    template_data = None

    if template_type == TemplateType.RECORD:
        template_data = {"extras": [extra]}
    else:
        template_data = [extra]

    template = new_template(type=template_type, data=template_data)
    shacl_data = get_export_data(template, const.EXPORT_TYPE_SHACL, user=dummy_user)

    assert shacl_data is not None

    graph = RDFGraph()
    graph.parse(shacl_data, format="turtle")

    template_ref = URIRef(url_for("templates.view_template", id=template.id))
    template_ns = Namespace(
        url_for("templates.view_template", id=template.id, _anchor="")
    )

    assert graph.value(template_ref, RDF.type) == SH.NodeShape

    extras_node = graph.value(template_ref, SH.property)

    assert isinstance(extras_node, BNode)
    assert graph.value(extras_node, SH.name) == Literal(extra["key"], lang="en")
    assert graph.value(extras_node, SH.maxCount) == Literal(1)

    if "validation" in extra:
        validation = extra["validation"]

        if "required" in validation:
            assert graph.value(extras_node, SH.minCount) == Literal(1)

        if "options" in validation:
            assert isinstance(graph.value(extras_node, SH["in"]), BNode)

    if is_nested_type(extra["type"]):
        key = quote(extra["key"], safe="")
        nested_extra_ref = template_ns[key]

        assert graph.value(template_ref, OWL.imports) == nested_extra_ref
        assert graph.value(extras_node, SH.qualifiedValueShape) == nested_extra_ref
        assert graph.value(nested_extra_ref, RDF.type) == SH.NodeShape
