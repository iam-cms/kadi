# Copyright 2024 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

import jsonref
from flask import json

import kadi.lib.constants as const
from kadi.modules.templates.imports import parse_import_data
from kadi.modules.templates.models import TemplateType
from tests.constants import DEFAULT_RECORD_TEMPLATE_DATA
from tests.constants import DUMMY_EXTRA_NO_VALUE
from tests.constants import DUMMY_EXTRA_VALUE


def test_parse_import_data_json():
    """Test if JSON import data is parsed correctly."""

    # Test parsing record template data.
    template_data = {
        "id": 123,
        "type": TemplateType.RECORD,
        "identifier": "test",
        "data": {
            "identifier": "test2",
            "extras": [DUMMY_EXTRA_VALUE],
        },
    }
    data = BytesIO(json.dumps(template_data).encode())

    assert parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.RECORD) == {
        **template_data,
        "data": {
            **DEFAULT_RECORD_TEMPLATE_DATA,
            "identifier": template_data["data"]["identifier"],
            "extras": template_data["data"]["extras"],
        },
    }

    data.seek(0)

    assert parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.EXTRAS) == {
        **template_data,
        "data": template_data["data"]["extras"],
    }

    # Test parsing extras template data.
    template_data = {
        "id": 123,
        "type": TemplateType.EXTRAS,
        "identifier": "test",
        "data": [DUMMY_EXTRA_VALUE],
    }
    data = BytesIO(json.dumps(template_data).encode())

    assert parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.RECORD) == {
        **template_data,
        "data": {"extras": template_data["data"]},
    }

    data.seek(0)

    assert (
        parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.EXTRAS)
        == template_data
    )

    # Test parsing record data.
    record_data = {
        "id": 123,
        "identifier": "test",
        "type": "test",
        "extras": [DUMMY_EXTRA_VALUE],
    }
    data = BytesIO(json.dumps(record_data).encode())

    assert parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.RECORD) == {
        "data": {**record_data, "extras": [DUMMY_EXTRA_NO_VALUE]}
    }

    data.seek(0)

    assert parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.EXTRAS) == {
        "data": [DUMMY_EXTRA_NO_VALUE]
    }


def test_parse_import_data_json_schema():
    """Test if JSON Schema import data is parsed correctly."""
    import_data = {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "type": "object",
        "properties": {
            "test": {"type": "string", "default": "test"},
        },
    }

    data = BytesIO(jsonref.dumps(import_data).encode())

    assert parse_import_data(
        data, const.IMPORT_TYPE_JSON_SCHEMA, TemplateType.RECORD
    ) == {"data": {**DEFAULT_RECORD_TEMPLATE_DATA, "extras": [DUMMY_EXTRA_VALUE]}}

    data.seek(0)

    assert parse_import_data(
        data, const.IMPORT_TYPE_JSON_SCHEMA, TemplateType.EXTRAS
    ) == {"data": [DUMMY_EXTRA_VALUE]}


def test_parse_import_data_shacl():
    """Test if SHACL (Turtle) import data is parsed correctly."""
    import_data = b"""
    @prefix dcterms: <http://purl.org/dc/terms/> .
    @prefix ex: <http://example.com/#> .
    @prefix sh: <http://www.w3.org/ns/shacl#> .
    @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

    <http://example.com/1> a sh:NodeShape ;
        dcterms:title "test"@en ;
        dcterms:description "test description"@en ;
        sh:property [ sh:datatype xsd:string ;
                sh:defaultValue "test"^^xsd:string ;
                sh:maxCount 1 ;
                sh:name "test"@en ;
                sh:order 0 ;
                sh:path ex:test ] .
    """

    data = BytesIO(import_data)

    template_data = {"title": "test", "description": "test description"}
    extra = {**DUMMY_EXTRA_VALUE, "term": "http://example.com/#test"}

    assert parse_import_data(data, const.IMPORT_TYPE_SHACL, TemplateType.RECORD) == {
        **template_data,
        "data": {**DEFAULT_RECORD_TEMPLATE_DATA, "extras": [extra]},
    }

    data.seek(0)

    assert parse_import_data(data, const.IMPORT_TYPE_SHACL, TemplateType.EXTRAS) == {
        **template_data,
        "data": [extra],
    }
