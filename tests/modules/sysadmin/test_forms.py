# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from werkzeug.datastructures import MultiDict

from kadi.lib.forms import BaseForm
from kadi.modules.sysadmin.forms import NavFooterField


@pytest.mark.parametrize(
    "data,is_valid,result",
    [
        (None, True, []),
        ({"test": '[["foo", "bar"]]'}, True, [["foo", "bar"]]),
        ({"test": '["foo", "bar"]'}, False, []),
        ({"test": '{"foo": "bar"}'}, False, []),
    ],
)
def test_nav_footer_field(data, is_valid, result):
    """Test if the custom "NavFooterField" works correctly."""

    class _TestForm(BaseForm):
        test = NavFooterField("Test")

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid
    assert form.test.data == result

    if not is_valid:
        assert "Invalid data structure." in form.errors["test"]
