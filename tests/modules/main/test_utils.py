# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import kadi.lib.constants as const
from kadi.lib.favorites.models import Favorite
from kadi.lib.search.models import SavedSearch
from kadi.lib.search.schemas import SavedSearchSchema
from kadi.lib.web import url_for
from kadi.modules.collections.schemas import CollectionSchema
from kadi.modules.main.utils import get_favorite_resources
from kadi.modules.main.utils import get_latest_resources
from kadi.modules.main.utils import get_resource_searches
from kadi.modules.records.schemas import RecordSchema


def test_get_favorite_resources(dummy_collection, dummy_record, dummy_user):
    """Test if favorited resources for the home page are retrieved correctly."""
    object_name = "record"
    record_meta = const.RESOURCE_TYPES[object_name]

    Favorite.create(user=dummy_user, object=object_name, object_id=dummy_record.id)

    assert get_favorite_resources(user=dummy_user) == [
        {
            **RecordSchema(_internal=True).dump(dummy_record),
            "pretty_type": str(record_meta["title"]),
        }
    ]


def test_get_resource_searches(dummy_user):
    """Test if resource searches for the home page are retrieved correctly."""
    object_name = "record"
    record_meta = const.RESOURCE_TYPES[object_name]

    saved_search = SavedSearch.create(
        user=dummy_user, name="test", object=object_name, query_string="foo=bar"
    )

    assert get_resource_searches(user=dummy_user) == [
        {
            **SavedSearchSchema(_internal=True).dump(saved_search),
            "pretty_type": str(record_meta["title_plural"]),
        }
    ]


def test_get_latest_resources(
    dummy_collection, dummy_group, dummy_template, dummy_record, dummy_user
):
    """Test if the latest resources for the home page are retrieved correctly."""
    record_meta = const.RESOURCE_TYPES["record"]
    collection_meta = const.RESOURCE_TYPES["collection"]

    assert get_latest_resources(user=dummy_user) == [
        {
            "title": str(record_meta["title_plural"]),
            "url": url_for(record_meta["endpoint"]),
            "items": [RecordSchema(_internal=True).dump(dummy_record)],
        },
        {
            "title": str(collection_meta["title_plural"]),
            "url": url_for(collection_meta["endpoint"]),
            "items": [CollectionSchema(_internal=True).dump(dummy_collection)],
        },
    ]
