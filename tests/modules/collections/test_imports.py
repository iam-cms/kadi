# Copyright 2024 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

from flask import json

import kadi.lib.constants as const
from kadi.modules.collections.imports import parse_import_data


def test_parse_import_data_json():
    """Test if JSON import data is parsed correctly."""
    collection_data = {"identifier": "test"}
    data = BytesIO(json.dumps(collection_data).encode())

    assert parse_import_data(data, const.IMPORT_TYPE_JSON) == collection_data
