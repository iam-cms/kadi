# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.resources.utils import add_link
from kadi.modules.collections.core import link_collections
from kadi.modules.collections.forms import NewCollectionForm


def test_new_collection_form_import_data(dummy_user):
    """Test if prefilling a "NewCollectionForm" with import data works correctly."""
    import_data = {"identifier": "test"}
    form = NewCollectionForm(import_data=import_data, user=dummy_user)

    assert form.identifier.data == import_data["identifier"]


def test_new_collection_form_collection(
    dummy_collection,
    dummy_record,
    dummy_template,
    dummy_user,
    new_collection,
    new_record,
    new_user,
):
    """Test if prefilling a "NewCollectionForm" with a collection works correctly."""
    collection = new_collection(tags=["test"], record_template=dummy_template.id)

    user = new_user()
    # This record should not appear in the linked records in the form.
    record = new_record(creator=user)
    add_link(collection.records, record, user=user)
    add_link(collection.records, dummy_record, user=dummy_user)
    link_collections(dummy_collection, collection, user=dummy_user)

    form = NewCollectionForm(collection=collection, user=dummy_user)

    tag = collection.tags.first().name

    assert form.identifier.data == collection.identifier
    assert form.tags.initial == [(tag, tag)]
    assert form.record_template.initial == (
        dummy_template.id,
        f"@{dummy_template.identifier}",
    )
    assert form.records.initial == [(dummy_record.id, f"@{dummy_record.identifier}")]
    assert form.parent_collection.initial == (
        dummy_collection.id,
        f"@{dummy_collection.identifier}",
    )
    assert form.roles.initial == []
