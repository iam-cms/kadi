# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import functools
import os
import zipfile
from io import BytesIO

import pytest
from flask import json
from rdflib import RDF
from rdflib import SDO
from rdflib import URIRef

import kadi.lib.constants as const
from kadi.lib.export import RDFGraph
from kadi.lib.resources.utils import add_link
from kadi.lib.web import url_for
from kadi.modules.collections.export import get_export_data


@pytest.mark.parametrize("exclude_records", [True, False])
def test_get_export_data_json(
    exclude_records, dummy_collection, dummy_record, dummy_user
):
    """Test if the collection JSON export works correctly."""
    add_link(dummy_collection.records, dummy_record, user=dummy_user)

    export_filter = {"user": True, "records": exclude_records}
    json_data = get_export_data(
        dummy_collection,
        const.EXPORT_TYPE_JSON,
        export_filter=export_filter,
        user=dummy_user,
    )

    assert json_data is not None

    collection_data = json.loads(json_data.read().decode())

    assert "creator" not in collection_data

    if exclude_records:
        assert "records" not in collection_data
    else:
        assert "records" in collection_data
        assert len(collection_data["records"]) == 1

        record_data = collection_data["records"][0]

        assert "creator" not in record_data
        assert "files" in record_data
        assert record_data["links"] == []


def test_get_export_data_qr(dummy_collection, dummy_user):
    """Test if the collection QR code export works correctly."""
    assert get_export_data(
        dummy_collection, const.EXPORT_TYPE_QR, user=dummy_user
    ).getvalue()


@pytest.mark.parametrize("exclude_records", [True, False])
def test_get_export_data_rdf(
    exclude_records, dummy_collection, dummy_record, dummy_user
):
    """Test if the collection RDF works correctly."""
    add_link(dummy_collection.records, dummy_record, user=dummy_user)

    export_filter = {"records": exclude_records}
    rdf_data = get_export_data(
        dummy_collection,
        const.EXPORT_TYPE_RDF,
        export_filter=export_filter,
        user=dummy_user,
    )

    assert rdf_data is not None

    graph = RDFGraph()
    graph.parse(rdf_data, format="turtle")

    collection_ref = URIRef(
        url_for("collections.view_collection", id=dummy_collection.id)
    )

    assert graph.value(collection_ref, RDF.type) == SDO.Collection

    record_nodes = list(graph.objects(collection_ref, SDO.hasPart))

    if exclude_records:
        assert len(record_nodes) == 0
    else:
        assert len(record_nodes) == 1

        record_ref = URIRef(url_for("records.view_record", id=dummy_record.id))

        assert record_nodes[0] == record_ref
        assert graph.value(record_ref, RDF.type) == SDO.Dataset


@pytest.mark.parametrize("metadata_only", [True, False])
@pytest.mark.parametrize("exclude_records", [True, False])
def test_get_export_data_ro_crate(
    metadata_only,
    exclude_records,
    dummy_collection,
    dummy_record,
    dummy_file,
    dummy_user,
    new_record,
):
    """Test if the collection RO-Crate export works correctly."""
    record = new_record()

    add_link(dummy_collection.records, dummy_record, user=dummy_user)
    add_link(dummy_collection.records, record, user=dummy_user)

    export_filter = {"records": exclude_records, "metadata_only": metadata_only}
    export_data = get_export_data(
        dummy_collection,
        const.EXPORT_TYPE_RO_CRATE,
        export_filter=export_filter,
        user=dummy_user,
    )

    assert export_data is not None

    if metadata_only:
        metadata = json.loads(export_data.read().decode())
        graph = metadata["@graph"]

        if exclude_records:
            assert len(graph) == 3
        else:
            assert len(graph) == 11

        index = 0

        assert graph[index]["@id"] == "ro-crate-metadata.json"
        assert graph[index]["@type"] == "CreativeWork"

        index += 1

        assert graph[index]["@id"] == "./"
        assert graph[index]["@type"] == "Dataset"
        assert graph[index]["name"] == dummy_collection.title

        if exclude_records:
            assert "hasPart" not in graph[index]
        else:
            assert len(graph[index]["hasPart"]) == 2

        index += 1

        if not exclude_records:
            assert graph[index]["@id"] == const.URL_INDEX
            assert graph[index]["@type"] == "Organization"

            index += 1

            assert graph[index]["@id"] == url_for(
                "accounts.view_user", id=dummy_user.id
            )
            assert graph[index]["@type"] == "Person"

            index += 1

            # The newly created record should be included first due to its timestamp.
            assert graph[index]["@id"] == f"./{record.identifier}/"
            assert graph[index]["@type"] == "Dataset"
            assert len(graph[index]["hasPart"]) == 2

            index += 1

            assert (
                graph[index]["@id"] == f"./{record.identifier}/{record.identifier}.json"
            )
            assert graph[index]["@type"] == "File"

            index += 1

            assert (
                graph[index]["@id"] == f"./{record.identifier}/{record.identifier}.ttl"
            )
            assert graph[index]["@type"] == "File"

            index += 1

            # And the dummy record with it's file afterwards.
            assert graph[index]["@id"] == f"./{dummy_record.identifier}/"
            assert graph[index]["@type"] == "Dataset"
            assert len(graph[index]["hasPart"]) == 3

            index += 1

            assert (
                graph[index]["@id"]
                == f"./{dummy_record.identifier}/{dummy_record.identifier}.json"
            )
            assert graph[index]["@type"] == "File"

            index += 1

            assert (
                graph[index]["@id"]
                == f"./{dummy_record.identifier}/{dummy_record.identifier}.ttl"
            )
            assert graph[index]["@type"] == "File"

            index += 1

            assert (
                graph[index]["@id"]
                == f"./{dummy_record.identifier}/files/{dummy_file.name}"
            )
            assert graph[index]["@type"] == "File"
    else:
        data = functools.reduce(lambda acc, val: acc + val, export_data)

        with zipfile.ZipFile(BytesIO(data)) as ro_crate:
            namelist = ro_crate.namelist()

            if exclude_records:
                assert len(namelist) == 1
            else:
                assert len(namelist) == 6

                root_dir = dummy_collection.identifier
                filepaths = [
                    os.path.join(root_dir, "ro-crate-metadata.json"),
                    os.path.join(
                        root_dir, record.identifier, f"{record.identifier}.json"
                    ),
                    os.path.join(
                        root_dir, record.identifier, f"{record.identifier}.ttl"
                    ),
                    os.path.join(
                        root_dir,
                        dummy_record.identifier,
                        f"{dummy_record.identifier}.json",
                    ),
                    os.path.join(
                        root_dir,
                        dummy_record.identifier,
                        f"{dummy_record.identifier}.ttl",
                    ),
                    os.path.join(
                        root_dir, dummy_record.identifier, "files", dummy_file.name
                    ),
                ]

                for filepath in filepaths:
                    assert filepath in ro_crate.namelist()
