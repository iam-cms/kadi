# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

import pytest
from flask import get_flashed_messages
from flask import json

import kadi.lib.constants as const
from kadi.lib.permissions.models import Role
from kadi.lib.resources.utils import add_link
from kadi.lib.web import url_for
from kadi.modules.collections.models import Collection
from kadi.modules.collections.models import CollectionState
from kadi.modules.collections.models import CollectionVisibility
from tests.utils import check_view_response


def test_collections(client, user_session):
    """Test the "collections.collections" endpoint."""
    with user_session():
        response = client.get(url_for("collections.collections"))
        check_view_response(response)


def test_new_collection_create(client, user_session):
    """Test the regular "collections.new_collection" endpoint."""
    endpoint = url_for("collections.new_collection")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "identifier": "test",
                "title": "test",
                "visibility": CollectionVisibility.PRIVATE,
            },
        )

        check_view_response(response, status_code=302)
        assert Collection.query.filter_by(identifier="test").one()


def test_new_collection_import(client, user_session):
    """Test the file import of the "collections.new_collection" endpoint."""
    import_data = {"identifier": "test"}

    with user_session():
        response = client.post(
            url_for("collections.new_collection"),
            data={
                "import_type": const.IMPORT_TYPE_JSON,
                "import_data": (BytesIO(json.dumps(import_data).encode()), "file"),
            },
            content_type=const.MIMETYPE_FORMDATA,
        )

        check_view_response(response)
        assert "File imported successfully." in get_flashed_messages()


def test_edit_collection(client, dummy_collection, user_session):
    """Test the "collections.edit_collection" endpoint."""
    endpoint = url_for("collections.edit_collection", id=dummy_collection.id)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"identifier": "test"})

        check_view_response(response, status_code=302)
        assert dummy_collection.identifier == "test"


def test_view_collection(client, dummy_collection, user_session):
    """Test the "collections.view_collection" endpoint."""
    with user_session():
        response = client.get(
            url_for("collections.view_collection", id=dummy_collection.id)
        )
        check_view_response(response)


@pytest.mark.parametrize("export_type", const.EXPORT_TYPES["collection"])
def test_export_collection(export_type, client, dummy_collection, user_session):
    """Test the "collections.export_collection" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "collections.export_collection",
                id=dummy_collection.id,
                export_type=export_type,
            )
        )
        check_view_response(response)


def test_publish_collection(client, dummy_collection, user_session):
    """Test the "collections.publish_collection" endpoint."""
    endpoint = url_for(
        "collections.publish_collection", id=dummy_collection.id, provider="zenodo"
    )

    with user_session():
        response = client.get(endpoint)
        check_view_response(response, status_code=200)

        response = client.post(endpoint)
        check_view_response(response, status_code=302)


def test_manage_links_records(client, dummy_collection, dummy_record, user_session):
    """Test the "records" tab of the "collections.manage_links" endpoint."""
    endpoint = url_for(
        "collections.manage_links", id=dummy_collection.id, tab="records"
    )

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"records": [dummy_record.id]})

        check_view_response(response)
        assert dummy_collection.records.one() == dummy_record


def test_manage_links_collections(
    client, dummy_collection, new_collection, user_session
):
    """Test the "collections" tab of the "collections.manage_links" endpoint."""
    collection = new_collection()
    endpoint = url_for(
        "collections.manage_links", id=dummy_collection.id, tab="collections"
    )

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"collections": [collection.id]})

        check_view_response(response)
        assert dummy_collection.children.one() == collection


def test_manage_permissions_collection(
    client, dummy_collection, dummy_group, new_user, user_session
):
    """Test the "collection" tab of the "collections.manage_permissions" endpoint."""
    endpoint = url_for(
        "collections.manage_permissions", id=dummy_collection.id, tab="collection"
    )
    new_role = "member"
    user = new_user()

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "roles": json.dumps(
                    [
                        {
                            "subject_type": "user",
                            "subject_id": user.id,
                            "role": new_role,
                        },
                        {
                            "subject_type": "group",
                            "subject_id": dummy_group.id,
                            "role": new_role,
                        },
                    ]
                )
            },
        )

        check_view_response(response, status_code=302)
        assert user.roles.filter(
            Role.object == "collection",
            Role.object_id == dummy_collection.id,
            Role.name == new_role,
        ).one()
        assert dummy_group.roles.filter(
            Role.object == "collection",
            Role.object_id == dummy_collection.id,
            Role.name == new_role,
        ).one()


def test_manage_permissions_records(
    client,
    dummy_collection,
    dummy_group,
    dummy_record,
    dummy_user,
    new_record,
    new_user,
    user_session,
):
    """Test the "records" tab of the "collections.manage_permissions" endpoint."""
    endpoint = url_for(
        "collections.manage_permissions", id=dummy_collection.id, tab="records"
    )
    new_role = "member"

    user_1 = new_user()
    user_2 = new_user()
    record = new_record(creator=user_2)

    # Link a record whose permissions can be changed.
    add_link(dummy_collection.records, dummy_record, user=dummy_user)
    # Link a record whose permissions cannot be changed.
    add_link(dummy_collection.records, record, user=user_2)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "roles": json.dumps(
                    [
                        {
                            "subject_type": "user",
                            "subject_id": user_1.id,
                            "role": new_role,
                        },
                        {
                            "subject_type": "group",
                            "subject_id": dummy_group.id,
                            "role": new_role,
                        },
                    ]
                )
            },
        )

        check_view_response(response, status_code=302)
        assert user_1.roles.filter(
            Role.object == "record",
            Role.object_id == dummy_record.id,
            Role.name == new_role,
        ).one()
        assert (
            user_1.roles.filter(
                Role.object == "record", Role.object_id == record.id
            ).first()
            is None
        )
        assert dummy_group.roles.filter(
            Role.object == "record",
            Role.object_id == dummy_record.id,
            Role.name == new_role,
        ).one()
        assert (
            dummy_group.roles.filter(
                Role.object == "record", Role.object_id == record.id
            ).first()
            is None
        )


def test_view_revision(client, dummy_collection, user_session):
    """Test the "collections.view_revision" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "collections.view_revision",
                collection_id=dummy_collection.id,
                revision_id=dummy_collection.ordered_revisions.first().id,
            )
        )
        check_view_response(response)


def test_delete_collection(client, dummy_collection, user_session):
    """Test the "collections.delete_collection" endpoint."""
    with user_session():
        response = client.post(
            url_for("collections.delete_collection", id=dummy_collection.id)
        )

        check_view_response(response, status_code=302)
        assert dummy_collection.state == CollectionState.DELETED
