# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import functools
import os
import zipfile
from io import BytesIO

import pytest
from flask import json
from rdflib import RDF
from rdflib import RDFS
from rdflib import SDO
from rdflib import XSD
from rdflib import BNode
from rdflib import Literal
from rdflib import Namespace
from rdflib import URIRef

import kadi.lib.constants as const
from kadi.lib.export import RDFGraph
from kadi.lib.web import url_for
from kadi.modules.records.export import get_extras_export_data
from kadi.modules.records.export import get_record_export_data
from kadi.modules.records.links import create_record_link


DUMMY_EXTRAS = [
    {
        "key": "list",
        "type": "list",
        "description": "A list.",
        "term": "https://example.com/list",
        "value": [
            {
                "type": "dict",
                "value": [
                    {
                        "key": "float",
                        "type": "float",
                        "value": 1.5,
                        "unit": "mm",
                    },
                    {
                        "key": "date",
                        "type": "date",
                        "value": "2023-03-22T10:21:29.261394+00:00",
                        "term": "https://example.com/date",
                    },
                ],
            },
        ],
    },
]


def test_get_record_export_data_json(dummy_record, dummy_user, new_record):
    """Test if the record JSON export works correctly."""
    create_record_link(
        name="out", record_from=dummy_record, record_to=new_record(), creator=dummy_user
    )
    create_record_link(
        name="in", record_from=new_record(), record_to=dummy_record, creator=dummy_user
    )

    export_filter = {"user": True, "links": "in"}
    json_data = get_record_export_data(
        dummy_record,
        const.EXPORT_TYPE_JSON,
        export_filter=export_filter,
        user=dummy_user,
    )

    assert json_data is not None

    record_data = json.loads(json_data.read().decode())

    assert "creator" not in record_data
    assert "files" in record_data
    assert "links" in record_data
    assert len(record_data["links"]) == 1

    link_data = record_data["links"][0]

    assert "creator" not in link_data

    linked_record_data = link_data["record_to"]

    assert "creator" not in linked_record_data
    assert "files" not in linked_record_data
    assert "links" not in linked_record_data


def test_get_record_export_data_pdf(dummy_file, dummy_record, dummy_user, new_record):
    """Test if the record PDF export works correctly."""
    record = new_record()
    create_record_link(
        name="test", record_from=dummy_record, record_to=record, creator=dummy_user
    )

    # Check handling of unicode characters and some extra metadata with a date.
    dummy_record.description = b"\xf0\x9f\x90\xb1".decode()
    dummy_record.extras = DUMMY_EXTRAS

    assert get_record_export_data(
        dummy_record, const.EXPORT_TYPE_PDF, user=dummy_user
    ).getvalue()


def test_get_record_export_data_qr(dummy_record, dummy_user):
    """Test if the record QR code export works correctly."""
    assert get_record_export_data(
        dummy_record, const.EXPORT_TYPE_QR, user=dummy_user
    ).getvalue()


def test_get_record_export_rdf(
    dummy_file, dummy_license, dummy_record, dummy_user, new_record
):
    """Test if the record RDF export works correctly."""
    dummy_record.type = "test"
    dummy_record.license = dummy_license
    dummy_record.extras = DUMMY_EXTRAS

    record = new_record()
    record_link = create_record_link(
        name="link",
        record_from=dummy_record,
        record_to=record,
        creator=dummy_user,
        term="http://example.org/link",
    )

    rdf_data = get_record_export_data(
        dummy_record, const.EXPORT_TYPE_RDF, user=dummy_user
    )

    assert rdf_data is not None

    graph = RDFGraph()
    graph.parse(rdf_data, format="turtle")

    record_ref = URIRef(url_for("records.view_record", id=dummy_record.id))
    record_ns = Namespace(
        url_for("records.view_record", id=dummy_record.id, _anchor="")
    )

    author_ref = URIRef(url_for("accounts.view_user", id=dummy_user.id))

    assert graph.value(record_ref, RDF.type) == SDO.Dataset
    assert graph.value(record_ref, SDO.author) == author_ref
    assert graph.value(record_ref, SDO.license) == URIRef(dummy_record.license.url)

    link_ref = URIRef(
        url_for(
            "records.view_record_link",
            record_id=dummy_record.id,
            link_id=record_link.id,
        )
    )

    assert graph.value(link_ref, RDF.type) == URIRef(record_link.term)
    assert graph.value(link_ref, RDF.subject) == record_ref
    assert graph.value(link_ref, RDF.object) == URIRef(
        url_for("records.view_record", id=record.id)
    )
    assert graph.value(link_ref, SDO.name) == Literal(record_link.name)
    assert graph.value(link_ref, SDO.author) == author_ref

    file_ref = URIRef(
        url_for("records.view_file", record_id=dummy_record.id, file_id=dummy_file.id)
    )

    assert graph.value(file_ref, RDF.type) == SDO.MediaObject
    assert graph.value(file_ref, SDO.isPartOf) == record_ref
    assert graph.value(file_ref, SDO.author) == author_ref

    extras_node = graph.value(record_ref, RDFS.isDefinedBy)

    assert isinstance(extras_node, BNode)

    list_node = graph.value(extras_node, URIRef("https://example.com/list"))

    assert isinstance(list_node, BNode)

    list_entry_node = graph.value(list_node, RDF.first)

    assert graph.value(list_node, RDF.rest) == RDF.nil

    dict_node = graph.value(list_entry_node, RDF.object)

    assert isinstance(dict_node, BNode)

    float_node = graph.value(dict_node, record_ns["float"])

    assert isinstance(float_node, BNode)
    assert graph.value(float_node, RDF.type) == SDO.QuantitativeValue
    assert graph.value(float_node, SDO.value) == Literal(1.5)
    assert graph.value(float_node, SDO.unitText) == Literal("mm")

    date_node = graph.value(dict_node, URIRef("https://example.com/date"))

    assert date_node == Literal(
        "2023-03-22T10:21:29.261394+00:00", datatype=XSD.dateTime
    )


@pytest.mark.parametrize("metadata_only", [True, False])
def test_get_record_export_data_ro_crate(
    metadata_only, dummy_file, dummy_license, dummy_record, dummy_user, new_record
):
    """Test if the record RO-Crate export works correctly."""
    dummy_record.license = dummy_license
    dummy_record.extras = DUMMY_EXTRAS

    export_filter = {"metadata_only": metadata_only}
    export_data = get_record_export_data(
        dummy_record,
        const.EXPORT_TYPE_RO_CRATE,
        export_filter=export_filter,
        user=dummy_user,
    )

    assert export_data is not None

    if metadata_only:
        metadata = json.loads(export_data.read().decode())
        graph = metadata["@graph"]

        assert len(graph) == 11

        index = 0

        assert graph[index]["@id"] == "ro-crate-metadata.json"
        assert graph[index]["@type"] == "CreativeWork"

        index += 1

        assert graph[index]["@id"] == "./"
        assert graph[index]["@type"] == "Dataset"
        assert len(graph[index]["hasPart"]) == 1
        assert graph[index]["name"] == dummy_record.title

        index += 1

        assert graph[index]["@id"] == const.URL_INDEX
        assert graph[index]["@type"] == "Organization"

        index += 1

        assert graph[index]["@id"] == url_for("accounts.view_user", id=dummy_user.id)
        assert graph[index]["@type"] == "Person"

        index += 1

        assert graph[index]["@id"] == dummy_record.license.url
        assert graph[index]["@type"] == "CreativeWork"

        index += 1

        assert (
            url_for("records.view_record", id=dummy_record.id, _anchor="extras")
            in graph[index]["@id"]
        )
        extra_url = url_for(
            "records.view_record", id=dummy_record.id, _anchor="extras-"
        )
        assert graph[index]["@id"] == f"{extra_url}list.0.float"
        assert graph[index]["@type"] == "PropertyValue"

        index += 1

        assert graph[index]["@id"] == f"{extra_url}list.0.date"
        assert graph[index]["@type"] == "PropertyValue"

        index += 1

        assert graph[index]["@id"] == f"./{dummy_record.identifier}/"
        assert graph[index]["@type"] == "Dataset"
        assert len(graph[index]["hasPart"]) == 3
        assert len(graph[index]["variableMeasured"]) == 2

        index += 1

        assert (
            graph[index]["@id"]
            == f"./{dummy_record.identifier}/{dummy_record.identifier}.json"
        )
        assert graph[index]["@type"] == "File"

        index += 1

        assert (
            graph[index]["@id"]
            == f"./{dummy_record.identifier}/{dummy_record.identifier}.ttl"
        )
        assert graph[index]["@type"] == "File"

        index += 1

        assert (
            graph[index]["@id"]
            == f"./{dummy_record.identifier}/files/{dummy_file.name}"
        )
        assert graph[index]["@type"] == "File"
    else:
        data = functools.reduce(lambda acc, val: acc + val, export_data)

        with zipfile.ZipFile(BytesIO(data)) as ro_crate:
            namelist = ro_crate.namelist()

            assert len(namelist) == 4

            root_dir = dummy_record.identifier
            filepaths = [
                os.path.join(root_dir, "ro-crate-metadata.json"),
                os.path.join(
                    root_dir, dummy_record.identifier, f"{dummy_record.identifier}.json"
                ),
                os.path.join(
                    root_dir, dummy_record.identifier, f"{dummy_record.identifier}.ttl"
                ),
                os.path.join(
                    root_dir, dummy_record.identifier, "files", dummy_file.name
                ),
            ]

            for filepath in filepaths:
                assert filepath in ro_crate.namelist()


def test_get_extras_export_data_json(dummy_record):
    """Test if the extras JSON export works correctly."""
    dummy_record.extras = DUMMY_EXTRAS

    export_filter = {"format": "plain"}
    json_data = get_extras_export_data(
        dummy_record, const.EXPORT_TYPE_JSON, export_filter=export_filter
    )

    assert json_data is not None

    extras_data = json.loads(json_data.read().decode())

    assert extras_data == {
        "list": [
            {
                "float": 1.5,
                "date": "2023-03-22T10:21:29.261394+00:00",
            },
        ]
    }
