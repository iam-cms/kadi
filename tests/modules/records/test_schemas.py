# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from marshmallow import ValidationError

import kadi.lib.constants as const
from kadi.modules.records.schemas import ChunkSchema
from kadi.modules.records.schemas import FileSchema


def test_file_schema(dummy_file, dummy_record):
    """Test if the "FileSchema" works correctly."""
    payload = {"name": dummy_file.name}

    FileSchema(only=["name"]).load(payload)
    FileSchema(only=["name"], record=dummy_record, previous_file=dummy_file).load(
        payload
    )

    with pytest.raises(ValidationError):
        FileSchema(only=["name"], record=dummy_record).load(payload)


def test_chunk_schema():
    """Test if the "ChunkSchema" works correctly."""
    ChunkSchema().load({"index": 1, "size": 1})
    ChunkSchema(chunk_count=1).load({"index": 0, "size": const.UPLOAD_CHUNK_SIZE})
    ChunkSchema(chunk_count=2).load({"index": 1, "size": 1})

    with pytest.raises(ValidationError):
        # Size is too large.
        ChunkSchema().load({"index": 0, "size": const.UPLOAD_CHUNK_SIZE + 1})
        # Size is too small, as it's not the last chunk.
        ChunkSchema(chunk_count=2).load({"index": 0, "size": 1})
        # Index is too high.
        ChunkSchema(chunk_count=1).load({"index": 1, "size": 1})
