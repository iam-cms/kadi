# Copyright 2024 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

from flask import json

import kadi.lib.constants as const
from kadi.modules.records.imports import parse_import_data
from kadi.modules.templates.models import TemplateType
from tests.constants import DUMMY_EXTRA_VALUE


def test_parse_import_data_json():
    """Test if JSON import data is parsed correctly."""

    # Test parsing record data.
    record_data = {"identifier": "test"}
    data = BytesIO(json.dumps(record_data).encode())

    assert parse_import_data(data, const.IMPORT_TYPE_JSON) == record_data

    # Test parsing record template data.
    template_data = {
        "type": TemplateType.RECORD,
        "identifier": "test",
        "data": {"identifier": "test2"},
    }
    data = BytesIO(json.dumps(template_data).encode())

    assert parse_import_data(data, const.IMPORT_TYPE_JSON) == template_data["data"]

    # Test parsing extras template data.
    template_data = {
        "type": TemplateType.EXTRAS,
        "identifier": "test",
        "data": [DUMMY_EXTRA_VALUE],
    }
    data = BytesIO(json.dumps(template_data).encode())

    assert parse_import_data(data, const.IMPORT_TYPE_JSON) == {
        "extras": template_data["data"]
    }
