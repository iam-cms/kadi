/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

import globals from 'globals';
import html from 'eslint-plugin-html';
import js from '@eslint/js';
import stylisticJs from '@stylistic/eslint-plugin-js';
import vue from 'eslint-plugin-vue';

export default [
  js.configs.recommended,
  ...vue.configs['flat/recommended'],
  {
    files: ['**/*.html'],
    plugins: {
      html,
    },
  },
  {
    plugins: {
      '@stylistic/js': stylisticJs,
    },
  },
  {
    languageOptions: {
      ecmaVersion: 2022,
      sourceType: 'module',
      globals: {
        ...globals.browser,
        ...globals.es2022,
        ...globals.jquery,
        kadi: 'readonly',
        $t: 'readonly',
        axios: 'readonly',
        dayjs: 'readonly',
        i18next: 'readonly',
      },
    },
  },
  {
    ignores: [
      'kadi/static',
      'kadi/templates/base.html',
    ],
  },
  {
    rules: {
      '@stylistic/js/array-bracket-newline': 'error',
      '@stylistic/js/array-bracket-spacing': 'error',
      '@stylistic/js/arrow-parens': 'error',
      '@stylistic/js/arrow-spacing': 'error',
      '@stylistic/js/block-spacing': 'error',
      '@stylistic/js/brace-style': 'error',
      '@stylistic/js/comma-dangle': [
        'error',
        'always-multiline',
      ],
      '@stylistic/js/comma-spacing': 'error',
      '@stylistic/js/comma-style': 'error',
      '@stylistic/js/computed-property-spacing': 'error',
      '@stylistic/js/dot-location': [
        'error',
        'property',
      ],
      '@stylistic/js/eol-last': 'error',
      '@stylistic/js/func-call-spacing': 'error',
      '@stylistic/js/function-call-argument-newline': [
        'error',
        'consistent',
      ],
      '@stylistic/js/function-paren-newline': 'error',
      '@stylistic/js/generator-star-spacing': 'error',
      '@stylistic/js/implicit-arrow-linebreak': 'error',
      '@stylistic/js/indent': [
        'error',
        2,
        {
          'SwitchCase': 1,
        },
      ],
      '@stylistic/js/key-spacing': 'error',
      '@stylistic/js/keyword-spacing': 'error',
      '@stylistic/js/linebreak-style': 'error',
      '@stylistic/js/max-len': [
        'error',
        {
          'code': 120,
        },
      ],
      '@stylistic/js/new-parens': 'error',
      '@stylistic/js/no-confusing-arrow': 'error',
      '@stylistic/js/no-floating-decimal': 'error',
      '@stylistic/js/no-mixed-operators': 'error',
      '@stylistic/js/no-multi-spaces': 'error',
      '@stylistic/js/no-multiple-empty-lines': [
        'error',
        {
          'max': 1,
        },
      ],
      '@stylistic/js/no-tabs': 'error',
      '@stylistic/js/no-trailing-spaces': 'error',
      '@stylistic/js/no-whitespace-before-property': 'error',
      '@stylistic/js/nonblock-statement-body-position': 'error',
      '@stylistic/js/object-curly-newline': 'error',
      '@stylistic/js/object-curly-spacing': 'error',
      '@stylistic/js/one-var-declaration-per-line': 'error',
      '@stylistic/js/operator-linebreak': [
        'error',
        'before',
      ],
      '@stylistic/js/padded-blocks': [
        'error',
        'never',
      ],
      '@stylistic/js/padding-line-between-statements': 'error',
      '@stylistic/js/quotes': [
        'error',
        'single',
      ],
      '@stylistic/js/rest-spread-spacing': [
        'error',
        'never',
      ],
      '@stylistic/js/semi': 'error',
      '@stylistic/js/semi-spacing': 'error',
      '@stylistic/js/semi-style': 'error',
      '@stylistic/js/space-before-blocks': 'error',
      '@stylistic/js/space-before-function-paren': [
        'error',
        'never',
      ],
      '@stylistic/js/space-in-parens': 'error',
      '@stylistic/js/space-infix-ops': 'error',
      '@stylistic/js/space-unary-ops': 'error',
      '@stylistic/js/spaced-comment': 'error',
      '@stylistic/js/switch-colon-spacing': 'error',
      '@stylistic/js/template-curly-spacing': 'error',
      '@stylistic/js/template-tag-spacing': 'error',
      '@stylistic/js/wrap-iife': 'error',
      '@stylistic/js/wrap-regex': 'error',
      '@stylistic/js/yield-star-spacing': 'error',
      'accessor-pairs': 'error',
      'array-callback-return': 'error',
      'block-scoped-var': 'error',
      'camelcase': [
        'error',
        {
          'properties': 'never',
        },
      ],
      'consistent-return': 'error',
      'curly': 'error',
      'default-case': 'error',
      'default-case-last': 'error',
      'default-param-last': 'error',
      'dot-notation': 'error',
      'eqeqeq': 'error',
      'func-name-matching': 'error',
      'func-style': [
        'error',
        'declaration',
        {
          'allowArrowFunctions': true,
        },
      ],
      'grouped-accessor-pairs': 'error',
      'init-declarations': 'error',
      'new-cap': 'error',
      'no-array-constructor': 'error',
      'no-bitwise': 'error',
      'no-caller': 'error',
      'no-console': [
        'error',
        {
          'allow': [
            'debug',
            'error',
            'info',
            'warn',
          ],
        },
      ],
      'no-constant-condition': [
        'error',
        {
          'checkLoops': false,
        },
      ],
      'no-constructor-return': 'error',
      'no-div-regex': 'error',
      'no-duplicate-imports': 'error',
      'no-else-return': 'error',
      'no-empty-function': 'error',
      'no-eq-null': 'error',
      'no-eval': 'error',
      'no-extend-native': 'error',
      'no-extra-bind': 'error',
      'no-extra-label': 'error',
      'no-implicit-coercion': 'error',
      'no-implicit-globals': 'error',
      'no-implied-eval': 'error',
      'no-iterator': 'error',
      'no-label-var': 'error',
      'no-labels': 'error',
      'no-lone-blocks': 'error',
      'no-loop-func': 'error',
      'no-loss-of-precision': 'error',
      'no-multi-str': 'error',
      'no-new-func': 'error',
      'no-new-object': 'error',
      'no-new-wrappers': 'error',
      'no-octal-escape': 'error',
      'no-param-reassign': 'error',
      'no-proto': 'error',
      'no-restricted-globals': 'error',
      'no-restricted-imports': 'error',
      'no-restricted-properties': 'error',
      'no-return-await': 'error',
      'no-script-url': 'error',
      'no-self-compare': 'error',
      'no-sequences': 'error',
      'no-template-curly-in-string': 'error',
      'no-throw-literal': 'error',
      'no-undef-init': 'error',
      'no-undefined': 'error',
      'no-unmodified-loop-condition': 'error',
      'no-unneeded-ternary': 'error',
      'no-unused-expressions': 'error',
      'no-use-before-define': 'error',
      'no-useless-backreference': 'error',
      'no-useless-call': 'error',
      'no-useless-computed-key': 'error',
      'no-useless-concat': 'error',
      'no-useless-constructor': 'error',
      'no-useless-rename': 'error',
      'no-useless-return': 'error',
      'no-var': 'error',
      'no-void': 'error',
      'object-shorthand': 'error',
      'operator-assignment': 'error',
      'prefer-const': 'error',
      'prefer-exponentiation-operator': 'error',
      'prefer-numeric-literals': 'error',
      'prefer-object-spread': 'error',
      'prefer-promise-reject-errors': 'error',
      'prefer-regex-literals': 'error',
      'prefer-rest-params': 'error',
      'prefer-spread': 'error',
      'prefer-template': 'error',
      'radix': 'error',
      'require-await': 'error',
      'sort-imports': [
        'error',
        {
          'allowSeparatedGroups': true,
        },
      ],
      'sort-vars': 'error',
      'symbol-description': 'error',
      'unicode-bom': 'error',
      'vars-on-top': 'error',
      'yoda': 'error',
      'vue/attribute-hyphenation': 'error',
      'vue/block-order': [
        'error',
        {
          'order': ['template', 'style', 'script'],
        },
      ],
      'vue/block-tag-newline': 'error',
      'vue/component-definition-name-casing': 'error',
      'vue/component-name-in-template-casing': [
        'error',
        'kebab-case',
      ],
      'vue/component-tags-order': 'off',
      'vue/first-attribute-linebreak': [
        'error',
        {
          'singleline': 'beside',
          'multiline': 'beside',
        },
      ],
      'vue/html-closing-bracket-newline': [
        'error',
        {
          'singleline': 'never',
          'multiline': 'never',
          'selfClosingTag': {
            'singleline': 'never',
            'multiline': 'never',
          },
        },
      ],
      'vue/html-closing-bracket-spacing': [
        'error', {
          'startTag': 'never',
          'endTag': 'never',
          'selfClosingTag': 'never',
        },
      ],
      'vue/html-end-tags': 'error',
      'vue/html-indent': 'error',
      'vue/html-quotes': 'error',
      'vue/html-self-closing': 'off',
      'vue/match-component-file-name': 'error',
      'vue/max-attributes-per-line': [
        'error',
        {
          'singleline': 10,
          'multiline': 1,
        },
      ],
      'vue/multi-word-component-names': 'off',
      'vue/multiline-html-element-content-newline': 'off',
      'vue/mustache-interpolation-spacing': 'error',
      'vue/no-multi-spaces': 'error',
      'vue/no-spaces-around-equal-signs-in-attribute': 'error',
      'vue/no-template-shadow': 'error',
      'vue/no-v-html': 'off',
      'vue/padding-line-between-blocks': 'error',
      'vue/prop-name-casing': 'error',
      'vue/require-default-prop': 'off',
      'vue/require-direct-export': 'error',
      'vue/require-prop-types': 'error',
      'vue/singleline-html-element-content-newline': 'off',
      'vue/this-in-template': 'error',
      'vue/v-bind-style': 'error',
      'vue/v-on-style': 'error',
      'vue/v-slot-style': 'error',
    },
  },
];
