#!/usr/bin/env bash
set -e

# Workaround to ensure that packages can be imported correctly when running in the CI.
if [[ -n $GITLAB_CI && -n "$NODE_PATH" ]]; then
  ln -fs $NODE_PATH .
fi

npm run --silent eslint "$@" --fix --max-warnings=0
