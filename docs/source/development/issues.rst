Common issues
=============

The Flask dev server and/or the webpack watcher are not picking up changes
--------------------------------------------------------------------------

Both the Flask development server and webpack use inotify to efficiently watch for any
file changes to automatically restart the server/rebuild the asset bundles. The number
of file watches that inotify can use may be limited by the operating system by default.
In that case, the limit can be increased permanently by running:

.. code-block:: bash

    echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

This amount should be enough in most cases. However, note that this might use more
(unswappable) kernel memory than before.

Code running inside a background task is not updated
----------------------------------------------------

When modifying Python code that is run as part of a background task via Celery and the
Celery worker itself is already running, it needs to be restarted to pick up any new
changes.

There is currently no built-in way to automatically restart the worker on code changes.
However, the code that is actually executed in the task should generally be runnable
outside of a task as well, so testing it independently is preferred before testing the
Celery integration.

Database migrations do not work correctly
-----------------------------------------

Especially when working on multiple branches with differing migration scripts, managing
database migrations might lead to various kinds of issues, sometimes without printing
any helpful error message at all when using the Kadi CLI.

The best way to fix these kinds of issues is to first identify the current revision that
is used by the database, e.g. by using the Kadi CLI:

.. code-block:: bash

    kadi migrations current

If no revision identifier is printed, it is also possible to directly query the
database:

.. code-block:: bash

    sudo -Hiu postgres psql -c "SELECT * FROM alembic_version" kadi

For this revision identifier, a corresponding migration script should exist somewhere,
which needs to be present to upgrade or downgrade the database to the desired version.
If this is not an option, e.g. if the script was deleted already, it might be easier to
simply recreate the database, as resetting it via the Kadi CLI likely won't work with a
broken migration script chain:

.. code-block:: bash

    sudo -Hiu postgres dropdb kadi
    sudo -Hiu postgres createdb -O kadi -E utf-8 -T template0 kadi

Afterwards, the database can be initialized again using one of the following commands:

.. code-block:: bash

    kadi db init        # Initialize the database
    kadi db sample-data # Initialize the database and setup some sample users and resources

One or more packages cannot be built when installing |kadi| with pip
--------------------------------------------------------------------

Some packages |kadi| depends on require being built from source, as they contain code
written in other programming languages. For many of these packages, pre-built binaries
are available, which will always be installed if available for the current combination
of Python version and operating system. Other packages always have to be built from
source, however, the required build prerequisites for these packages should usually be
installed already when following the :ref:`installation instructions
<installation-development-installation>`.

Therefore, if any package fails to build due to e.g. using a different operating system,
please see the respective package documentation in order to identify and install any
missing buld prerequisites.

The port of the Flask development server is already in use
----------------------------------------------------------

If port 5000 is already being used by another program running on a development machine,
the Flask development server (usually started via the ``kadi run`` command) will refuse
to start. There are two ways to fix this problem:

* **Freeing up the port:** This requires first identifying the program that is currently
  occupying the port. The best way to do so depends on the operating system, e.g. on
  Unix, the ``ss -lptn`` command may be used. Once identified, the corresponding program
  should be closed to free up the port, if possible.

  .. tip::
    On macOS, the Apple AirPlay service is listening on port 5000 by default, which can
    simply be deactivated if not required.

* **Using another port:** If the solution above is not an option, it is also possible to
  bind the development server to another port, e.g. by running:

  .. code-block:: bash

    kadi run -p 5001

  Note that this also requires adapting the :confval:`SERVER_NAME` that the development
  configuration of |kadi| uses by default, e.g. to ``"localhost:5001"`` when using the
  port of the example above. Please see the |kadi| :ref:`configuration section
  <installation-development-installation-configuration-kadi4mat>` in the development
  installation instructions for a reminder.
