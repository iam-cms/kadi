.. _apiref-lib:

Library
=======

This section contains a complete API reference of the :mod:`kadi.lib` Python module.

API
---

.. automodule:: kadi.lib.api.blueprint

.. automodule:: kadi.lib.api.core

.. automodule:: kadi.lib.api.models
    :exclude-members: AccessTokenMixin, query

.. automodule:: kadi.lib.api.schemas

.. automodule:: kadi.lib.api.utils

Cache
-----

.. automodule:: kadi.lib.cache

Config
------

.. automodule:: kadi.lib.config.core

.. automodule:: kadi.lib.config.models

Conversion
----------

.. automodule:: kadi.lib.conversion

Database
--------

.. automodule:: kadi.lib.db

Exceptions
----------

.. automodule:: kadi.lib.exceptions

Export
------

.. automodule:: kadi.lib.export

Favorites
---------

.. automodule:: kadi.lib.favorites.core

.. automodule:: kadi.lib.favorites.models

Federation
----------

.. automodule:: kadi.lib.federation

Format
------

.. automodule:: kadi.lib.format

Forms
-----

.. automodule:: kadi.lib.forms

Jinja
-----

.. automodule:: kadi.lib.jinja

Licenses
--------

.. automodule:: kadi.lib.licenses.core

.. automodule:: kadi.lib.licenses.models

.. automodule:: kadi.lib.licenses.schemas

.. automodule:: kadi.lib.licenses.utils

LDAP
----

.. automodule:: kadi.lib.ldap

Mails
-----

.. automodule:: kadi.lib.mails.core

.. automodule:: kadi.lib.mails.tasks

.. automodule:: kadi.lib.mails.utils

Notifications
-------------

.. automodule:: kadi.lib.notifications.core

.. automodule:: kadi.lib.notifications.models

.. automodule:: kadi.lib.notifications.schemas

OAuth
-----

.. automodule:: kadi.lib.oauth.core

.. automodule:: kadi.lib.oauth.models

.. automodule:: kadi.lib.oauth.schemas

.. automodule:: kadi.lib.oauth.utils

Permissions
-----------

.. automodule:: kadi.lib.permissions.core

.. automodule:: kadi.lib.permissions.models

.. automodule:: kadi.lib.permissions.schemas

.. automodule:: kadi.lib.permissions.tasks

.. automodule:: kadi.lib.permissions.utils

Plugins
-------

.. automodule:: kadi.lib.plugins.core

.. automodule:: kadi.lib.plugins.utils

Publication
-----------

.. automodule:: kadi.lib.publication

Resources
---------

.. automodule:: kadi.lib.resources.api

.. automodule:: kadi.lib.resources.core

.. automodule:: kadi.lib.resources.forms

.. automodule:: kadi.lib.resources.mappings

.. automodule:: kadi.lib.resources.schemas

.. automodule:: kadi.lib.resources.tasks

.. automodule:: kadi.lib.resources.utils

.. automodule:: kadi.lib.resources.views

Revisions
---------

.. automodule:: kadi.lib.revisions.core

.. automodule:: kadi.lib.revisions.models

.. automodule:: kadi.lib.revisions.schemas

.. automodule:: kadi.lib.revisions.utils

Schemas
-------

.. automodule:: kadi.lib.schemas

Search
------

.. automodule:: kadi.lib.search.core

.. automodule:: kadi.lib.search.models

.. automodule:: kadi.lib.search.schemas

Security
--------

.. automodule:: kadi.lib.security

Storage
-------

.. automodule:: kadi.lib.storage.core

.. automodule:: kadi.lib.storage.local

.. automodule:: kadi.lib.storage.misc

Tags
----

.. automodule:: kadi.lib.tags.core

.. automodule:: kadi.lib.tags.models

.. automodule:: kadi.lib.tags.schemas

Tasks
-----

.. automodule:: kadi.lib.tasks.core

.. automodule:: kadi.lib.tasks.models

.. automodule:: kadi.lib.tasks.utils

Utils
-----

.. automodule:: kadi.lib.utils

Validation
----------

.. automodule:: kadi.lib.validation

Web
---

.. automodule:: kadi.lib.web
