.. _installation-development:

Development
===========

This section describes how to :ref:`install <installation-development-installation>` and
:ref:`update <installation-development-updating>` a |kadi| environment suitable for
development. For setting up |kadi| as a service in a production environment, please
refer to the :ref:`production installation <installation-production>` instructions
instead.

.. note::
    As development is currently taking place mainly under Linux, specifically
    Debian-based Linux distributions such as **Debian** and **Ubuntu**, all instructions
    are based on the assumption that one of these systems is used.

    On Windows, it is also possible to use one of these systems via the **Windows
    Subsystem for Linux (WSL)**. Please see the chapter explaining how to :ref:`develop
    under WSL <installation-development-wsl>` for some WSL-specific considerations.

.. toctree::
    :maxdepth: 1

    installation
    updating
    python
    docker
    wsl
