.. _installation-development-updating:

Updating the application
========================

Whenever updating the application code via git (see also the :ref:`initial installation
<installation-development-installation>`), the following commands may have to be run
again, depending on the actual code that has been changed since the last update:

.. code-block:: bash

    pip install -U pip    # (Optional) Make sure the newest version of pip is being used
    pip install -e .[dev] # Install any new backend dependencies
    kadi db upgrade       # Upgrade the database schema
    kadi assets dev       # Install any new frontend dependencies and rebuild the assets
    kadi i18n compile     # (Optional) Recompile any new backend translations

While mainly intended for productive installations, it is also recommended to take a
look at the :ref:`update notes <installation-production-updating-notes>`, since some of
the listed changes may also be relevant for development environments.
