.. _installation-production-security:

Security considerations
=======================

When deploying |kadi| in production, there are certain security considerations to take
into account, which may depend on how |kadi| is installed. The steps and default
configurations that are part of the installation instructions described in this
documentation, as well as corresponding helper tools and scripts, should already provide
a secure foundation. However, note that certain aspects such as authentication or
encryption depend on how |kadi| is :ref:`configured <installation-configuration>`.

Besides the installation of |kadi| itself, including regular :ref:`updates
<installation-production-updating>` of the application, the server that the application
runs on should be secured and maintained in an appropriate manner as well. This may
include secure firewall settings, regular security updates of all installed packages and
services that |kadi| depends on as well as :ref:`backups
<installation-production-backup>`.

Security features
-----------------

While absolute security can never be guaranteed, |kadi| itself already offers various
features to aid with ensuring a secure environment, independent of installation or
deployment method. These currently include:

* Support of established authentication mechanisms, including OpenID Connect, Shibboleth
  and LDAP.
* Secure storage of passwords (scrypt) for local accounts.
* Fine-grained access permissions on a per-resource basis.
* Token-based API authorization using personal access tokens (PATs) or OAuth 2.0 via the
  Authorization Code Grant with support for PKCE.
* Consistent and enforced use of HTTPS, including HTTP Strict Transport Security (HSTS).
* Protection against Cross-Site Request Forgery (CSRF).
* Limited and secure use of cookies, including signatures and session protection.
* Rate limiting to prevent brute forcing of passwords and to aid against
  Denial-of-Service (DoS) attacks.
* Strong Content Security Policy (CSP), use of security headers and strict settings
  (e.g. when rendering Markdown) to prevent Cross-Site Scripting (XSS) and similar
  vulnerabilities.
* Strict referrer policy to protect the privacy of users.
* No use of externally loaded scripts or other resources.
* Use of established and well-tested libraries for all critical functionality.
* Extensive test suite for most of the backend functionality.
* Continuous Integration (CI) infrastructure for automated testing and to ensure a
  consistent code style and quality.

Please also see the current `security policy
<https://gitlab.com/iam-cms/kadi/-/blob/master/SECURITY.md>`__ of |kadi|.
