Federation
==========

This section explains all configuration items related to federation, which refers to the
functionality of connecting multiple |kadi| instances. Currently, this mainly
facilitates federated search of different resources stored in one or more "external"
|kadi| instances from within another instance of |kadi|. The central configuration is
done via the :confval:`FEDERATED_INSTANCES` configuration item, as explained below.

.. note::
    Using federation requires an account in all |kadi| instances that should be
    connected. This is the case for both the system administrator, initially configuring
    the connection, and the users of this functionality.

.. confval:: FEDERATED_INSTANCES

    A dictionary specifying one or more external |kadi| instances to enable for
    federated search in the following form:

    .. code-block:: python3

        {
            "example": {
                "title": "Kadi4Mat Example",
                "url": "https://kadi4mat.example.edu",
                "client_id": "<client_id>",
                "client_secret": "<client_secret>",
            },
        }

    Each dictionary key specifies a unique :confval:`name` (here: ``example``) of the
    external |kadi| instance, mapping to the instance-specific configuration. The
    respective configuration items are described in the next section.

    Defaults to ``{}``.

Instance configuration
----------------------

The following configuration options have to be specified for each external |kadi|
instance:

.. confval:: title
    :noindex:

    The title of the external |kadi| instance which will be displayed to users within
    the GUI of |kadi|.

    Defaults to the unique :confval:`name` of the corresponding instance.

.. confval:: url
    :noindex:

    The full URL of the external |kadi| instance.

    Defaults to ``None``.

.. confval:: client_id
    :noindex:

    The client ID required to perform the OAuth2 authorization flow with the external
    |kadi| instance. It can be obtained (together with the client secret) directly via
    the GUI of the respective external |kadi| instance by navigating to *Settings >
    Applications* and creating a new application. Besides specifying some general
    application metadata, the following settings are necessary for the integration to
    work correctly:

     * **Redirect URIs:** Must be in the form of
       ``https://<server_name>/settings/services/authorize/<name>``, where the
       ``<server_name>`` placeholder corresponds to the configured
       :confval:`SERVER_NAME` of |kadi|, while the ``<name>`` placeholder corresponds to
       the unique :confval:`name` of the respective and configured external |kadi|
       instance.
     * **Scopes:** The scopes required to search all supported resources are
       ``record.read``, ``collection.read`` and ``template.read``. In addition, the
       ``user.read`` scope is required if information about the creators of resources
       should be shown.

    Defaults to ``None``.

.. confval:: client_secret
    :noindex:

    The client secret required to perform the OAuth2 authorization flow with the
    external |kadi| instance. See the :confval:`client_id` configuration option on how
    to obtain it.

    Defaults to ``None``.
