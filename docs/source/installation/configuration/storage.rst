.. _installation-configuration-storage:

Storage
=======

This section explains all configuration items related to storage of record file and
upload data.

.. confval:: STORAGE_PROVIDER

    This configuration value specifies the type of the storage provider used for storing
    file data and corresponding upload data. By default, |kadi| uses the ``local``
    storage provider, however, additional storage providers may be supported via
    suitable plugins. Please see the sections below for all storage provider-specific
    information.

    Defaults to ``"local"``.

    .. note::
        Note that changing this value **after** some data has already been stored will
        not automatically migrate data to the new storage provider. Please see the
        section on :ref:`data migration <installation-configuration-storage-migration>`.

.. confval:: UPLOAD_USER_QUOTA

    The maximum allowed total size of a user's files and uploads, independent of where
    they are stored. Setting this configuration value to ``None`` will remove the size
    limit altogether.

    Defaults to ``10_000_000_000`` (10 GB).

Provider configuration
----------------------

Local
~~~~~

The ``local`` storage provider uses a regular file system which needs to be locally
accessible by |kadi|, either directly or via a network, e.g. using NFS.

The following configuration values are supported by this provider:

.. confval:: STORAGE_PATH

    An absolute local file path in which all data is stored in, e.g.
    ``"/opt/kadi/storage"``.

    Note that the corresponding directory structure will be created automatically by the
    application. If you want to create it beforehand, make sure the permissions of at
    least the storage directory are set up according to the user and group under which
    the application is running, e.g.:

    .. code-block:: bash

        sudo chown kadi:kadi /opt/kadi/storage
        sudo chmod 750 /opt/kadi/storage

    Defaults to ``None``.

    .. note::
        Note that changing this value **after** performing a production setup requires
        some changes to the generated Apache configuration file as well. Namely, the
        corresponding ``XSendFilePath`` directive needs to be adapted according to the
        new path. Please also refer back to the :ref:`manual installation
        <installation-production-manual-apache>` instructions.

Others
~~~~~~

If local storage is not suitable, other storage providers may be used via suitable
:ref:`plugins <installation-plugins>`, such as the first-party :ref:`S3 plugin
<installation-plugins-s3>` to integrate an S3-compatible object store with |kadi|. Note
that each plugin uses its own, plugin-specific configuration.

.. _installation-configuration-storage-migration:

Data migration
--------------

When changing the configured :confval:`STORAGE_PROVIDER` after some data has already
been stored in |kadi|, the existing data can optionally be migrated to the new storage
provider. For this to work, all relevant storage providers need to be configured
simultaneously, independent of the currently configured :confval:`STORAGE_PROVIDER`,
e.g. for the local storage provider, a valid :confval:`STORAGE_PATH` needs to be
configured.

To perform the actual migration, the Kadi CLI can be used:

.. code-block:: bash

    sudo su - kadi                # Switch to the kadi user (in production environments)
    kadi files migrate <provider> # Migrate all data to the specified storage provider

Even if not choosing to migrate the data, |kadi| will continue to work as-is, as long as
all relevant storage providers are configured correctly. In this case, storage providers
outside of the configured main :confval:`STORAGE_PROVIDER` can be treated as
"read-only", as all newly uploaded data will automatically end up in the storage
provider specified via the :confval:`STORAGE_PROVIDER` configuration option.
