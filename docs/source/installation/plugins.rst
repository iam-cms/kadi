.. _installation-plugins:

Plugins
=======

This section explains how plugins can be installed and configured to extend the
functionality of |kadi|. It is recommended to have a working |kadi|
:ref:`installation <installation>` before reading through this section. For general
information about configuring |kadi|, please also see :ref:`Configuration
<installation-configuration>`.

Installing plugins
------------------

Plugins can either be part of the main application package or implemented as independent
Python packages, which have to be installed separately in the same virtual environment
that |kadi| itself was installed in. In both cases, plugins are configured via the
:confval:`PLUGINS <PLUGINS; PLUGIN_CONFIG>` configuration value of |kadi|:

.. code-block:: python3

    PLUGINS = ["example", "another-example"]

This will activate the plugins with the names ``example`` and ``another-example`` in the
application.

Some plugins might require additional configuration to work correctly or to change how
they behave. This configuration is plugin-specific and can be specified via the
:confval:`PLUGIN_CONFIG <PLUGINS; PLUGIN_CONFIG>` configuration value of |kadi|. Note
that the configuration always needs to be a dictionary, e.g. like in the following
configuration for the ``example`` plugin:

.. code-block:: python3

    PLUGIN_CONFIG = {
        "example": {
            "example_key": "example_value",
        },
    }

First-party plugins
-------------------

Currently, all first-party plugins are part of the main application package and are
therefore available automatically after installing |kadi|. The following sections
describe each plugin in more detail.

Zenodo
~~~~~~

The ``zenodo`` plugin allows users to connect their |kadi| account to a `Zenodo
<https://zenodo.org>`__ account in order to upload records and collections directly to
the Zenodo repository.

A minimal plugin configuration may look like the following:

.. code-block:: python3

    PLUGIN_CONFIG = {
        "zenodo": {
            "client_id": "<client_id>",
            "client_secret": "<client_secret>",
        }
    }

The following configuration values are supported by this plugin:

.. confval:: client_id
    :noindex:

    The client ID required to perform the OAuth2 authorization flow with Zenodo. It can
    be obtained (together with the client secret) directly via Zenodo by navigating to
    *Applications > Developer applications* in the account settings page and creating a
    new application. Besides specifying some general application metadata, the following
    settings are necessary for the integration to work correctly:

     * **Redirect URIs:** Must be in the form of
       ``https://<server_name>/settings/services/authorize/zenodo``, where the
       ``<server_name>`` placeholder corresponds to the configured
       :confval:`SERVER_NAME` of |kadi|.
     * **Client type:** Should be set to ``Confidential``.

    Defaults to ``None``.

.. confval:: client_secret
    :noindex:

    The client secret required to perform the OAuth2 authorization flow with Zenodo. See
    the :confval:`client_id` configuration option on how to obtain it.

    Defaults to ``None``.

.. confval:: base_url
    :noindex:

    The URL of the Zenodo instance to use. For testing purposes, the `Zenodo sandbox
    <https://sandbox.zenodo.org>`__ can be used instead by configuring
    ``"https://sandbox.zenodo.org"``.

    Defaults to ``"https://zenodo.org"``.

.. confval:: warning_msg
    :noindex:

    An additional warning message that will be shown at the top of the publication page
    of individual resources.

    Defaults to ``None``.

.. _installation-plugins-s3:

S3
~~

The ``s3`` plugin provides file storage using an S3-compatible object store such as
`MinIO <https://min.io>`__ or `Amazon S3 <https://aws.amazon.com/s3>`__. The
corresponding ``s3`` storage provider can be configured via :confval:`STORAGE_PROVIDER`.
For general information about using different storage providers in |kadi|, please refer
to the :ref:`storage configuration <installation-configuration-storage>`.

A minimal plugin configuration may look like the following:

.. code-block:: python3

    PLUGIN_CONFIG = {
        "s3": {
            "endpoint_url": "https://s3.example.com",
            "bucket_name": "kadi",
            "access_key": "<s3_access_key>",
            "secret_key": "<s3_secret_key>",
        },
    }

The following configuration values are supported by this plugin:

.. confval:: endpoint_url
    :noindex:

    The base URL of the object store.

    Defaults to ``None``.

.. confval:: bucket_name
    :noindex:

    The name of a bucket the storage provider operates in, e.g. ``"kadi"``.

    Defaults to ``None``.

.. confval:: access_key
    :noindex:

    The access key used for authentication. It is recommended to restrict access to the
    bucket as configured via :confval:`bucket_name`. If not provided, the standard
    configuration locations of the `Boto3
    <https://boto3.amazonaws.com/v1/documentation/api/latest/index.html>`__ client
    library will be checked for this value instead.

    Defaults to ``None``.

.. confval:: secret_key
    :noindex:

    The secret key used for authentication. If not provided, the standard configuration
    locations of the `Boto3
    <https://boto3.amazonaws.com/v1/documentation/api/latest/index.html>`__ client
    library will be checked for this value instead.

    Defaults to ``None``.

.. confval:: region_name
    :noindex:

    The region name to use when accessing the object store. Only relevant for certain
    object store implementations.

    Defaults to ``None``.

.. confval:: signature_version
    :noindex:

    The signature version to use for signing requests. Only relevant for certain object
    store implementations.

    Defaults to ``None``.

.. confval:: use_presigned_urls
    :noindex:

    Whether to generate presigned URLs when downloading files. This allows direct file
    downloads via the underlying object store, including support for range requests, but
    requires the object store to be reachable externally.

    Defaults to ``True``.

.. confval:: presigned_url_expiration
    :noindex:

    The expiration time in seconds to use for all generated, presigned URLs if
    :confval:`use_presigned_urls` is ``True``.

    Defaults to ``60``.

.. confval:: config_kwargs
    :noindex:

    A dictionary of additional configuration values that will be passed as keyword
    arguments to the configuration object of the `Botocore
    <https://botocore.amazonaws.com/v1/documentation/api/latest/reference/config.html>`__
    client library.

    Defaults to ``{}``.

    .. note::
        This advanced configuration may be particularly useful to fix incompatibilities
        between the S3 client and non-AWS S3 implementations. A common example are
        differences in default integrity protections, which usually manifest in errors
        related to missing or invalid checksums. These may be worked around by defining
        the following configuration values:

        .. code-block:: python

            {
                "request_checksum_calculation": "when_required",
                "response_checksum_validation": "when_required",
            }

TIB TS
~~~~~~

The ``tib_ts`` plugin provides a search for term IRIs using the `TIB Terminology Service
<https://terminology.tib.eu/ts>`__ in all places where term IRIs can be specified.

The following configuration values are supported by this plugin:

.. confval:: endpoint
    :noindex:

    The term search API endpoint provided by the service.

    Defaults to ``"https://service.tib.eu/ts4tib/api/search"``.

InfluxDB
~~~~~~~~

The ``influxdb`` plugin provides a proxy connection to one or more `InfluxDB
<https://www.influxdata.com>`__ instances. Once set up, users will be able to configure
their connection to a specific InfluxDB instance by providing a corresponding API token
in their user preferences, which is stored encrypted in the database of |kadi|.
Afterwards, the InfluxDB instance can be queried using the proxy endpoint
``/api/influxdb/<name>/query?orgID=<orgID>`` of |kadi|, with the ``<name>`` replaced by
the corresponding name of the configured InfluxDB instance and the ``<orgID>`` replaced
by an organization ID within InfluxDB.

A minimal plugin configuration may look like the following:

.. code-block:: python3

    PLUGIN_CONFIG = {
        "influxdb": {
            "<name>": {
                "url": "https://influxdb.example.com",
            }
        }
    }

In contrast to other plugins, each InfluxDB instance has its own configuration and is
itself specified via a unique :confval:`name` as dictionary key, mapping to the
instance-specific configuration.

The following configuration values are supported by each instance:

.. confval:: url
    :noindex:

    The base URL (i.e. without ``/api/...``) of the InfluxDB instance to use.

    Defaults to ``None``.

.. confval:: title
    :noindex:

    The title which will be displayed to users when configuring the InfluxDB instance
    via the GUI of |kadi|.

    Defaults to the :confval:`name` of the corresponding InfluxDB instance.

.. confval:: timeout
    :noindex:

    The maximum amount of seconds a proxied request to the InfluxDB instance can last
    before it is aborted.

    Defaults to ``10``.

.. confval:: token
    :noindex:

    A global token to use for all proxied requests to the InfluxDB instance. Takes
    precendence over user tokens that have been configured previously.

    Defaults to ``None``.

    .. warning::
        Be mindful about the scopes and buckets that are accessible with the provided
        global token.

.. confval:: groups
    :noindex:

    A single group ID or a list of group IDs within |kadi| to restrict proxied access to
    the InfluxDB instance to. It is highly recommended to only specify trusted groups
    which are manageable by the sysadmin. Can be useful in combination with a global
    :confval:`token`.

    Defaults to ``None``.
