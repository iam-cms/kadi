# Kadi4Mat

**Kadi4Mat**, or **Kadi** for short, is a generic and open source virtual
research environment. Originally developed in the context of materials science,
Kadi4Mat can be used for the management of any type of research data within
different research disciplines and use cases. For more information about the
project, please see its [website](https://kadi.iam.kit.edu) and
[documentation](https://kadi.readthedocs.io/en/stable).

## Installation

While the packaged code of Kadi4Mat can easily be installed as a Python package
via [pip](https://pypi.org/project/kadi), a complete installation requires a
few additional dependencies and considerations. Please refer to the [stable
documentation](https://kadi.readthedocs.io/en/stable) for full installation
instructions.

## Development

Contributions to the code are always welcome. However, please consider creating
an issue first, as described below, if you are planning to make larger changes.
Please refer to the [latest
documentation](https://kadi.readthedocs.io/en/latest) for instructions on how
to set up a development environment of Kadi4Mat as well as other useful
information, such as how to set up a separate fork of the [main
repository](https://gitlab.com/iam-cms/kadi).

In order to merge any contributions back into the main repository, please open
a corresponding [merge
request](https://gitlab.com/iam-cms/kadi/-/merge_requests). Typically, the
source branch of the merge request would be a separate (feature) branch of your
forked repository containing the changes to merge, while the target branch
should correspond to the `master` branch of the main repository. Depending on
the changes, please make sure to add appropriate tests, documentation,
translations, etc. and also add a corresponding entry to the changelog in
[`HISTORY.md`](https://gitlab.com/iam-cms/kadi/-/blob/master/HISTORY.md), if
necessary. Furthermore, you can add yourself as a contributor to
[`AUTHORS.md`](https://gitlab.com/iam-cms/kadi/-/blob/master/AUTHORS.md).

## Issues

For any issues regarding Kadi4Mat (bugs, suggestions, discussions, etc.) please
use the [issue tracker](https://gitlab.com/iam-cms/kadi/-/issues) of this
project. Make sure to add one or more fitting labels to each issue in order to
keep them organized. Before creating a new issue, please also check whether a
similar issue is already open. Note that creating or interacting with issues
requires a GitLab account.

For **bugs** in particular, please use the provided [`Bug`
template](https://gitlab.com/iam-cms/kadi/-/issues/new?issuable_template=Bug)
when creating a new issue, which also adds the `Bug` label to the issue
automatically. For **security-related** issues or concerns, please see
[`SECURITY.md`](https://gitlab.com/iam-cms/kadi/-/blob/master/SECURITY.md).
